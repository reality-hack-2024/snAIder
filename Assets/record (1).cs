using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using HuggingFace.API;
public class record : MonoBehaviour
{
    public Button startButton;
    public TextMeshProUGUI text;

    AudioClip clip;
    byte[] bytes;
    bool recording;
    // Start is called before the first frame update
    void Start()
    {
        //set start button bg to green
        startButton.onClick.AddListener(startRecord);
        Debug.Log("starting recording");
        recording = false;
    }

    // Update is called once per frame
    void Update()
    {
 
        if (recording && Microphone.GetPosition(null) >= clip.samples)
        {
            //set start button bg to red
            StopRecording();
        }

    }

    private void StopRecording()
    {
        Debug.Log("stop recording");
        recording = false;
        var position = Microphone.GetPosition(null);
        Microphone.End(null);
        var samples = new float[position * clip.channels];
        clip.GetData(samples, 0);
        bytes = EncodeAsWAV(samples, clip.frequency, clip.channels);
        SendRecording();
    }

    void startRecord()
    {
        text.color = Color.white;
        text.text = "Recording...";
        clip = Microphone.Start(null, false, 5, 44100);
        recording = true;

    }

    private byte[] EncodeAsWAV(float[] samples, int frequency, int channels)
    {
        using (var memoryStream = new MemoryStream(44 + samples.Length * 2))
        {
            using (var writer = new BinaryWriter(memoryStream))
            {
                writer.Write("RIFF".ToCharArray());
                writer.Write(36 + samples.Length * 2);
                writer.Write("WAVE".ToCharArray());
                writer.Write("fmt ".ToCharArray());
                writer.Write(16);
                writer.Write((ushort)1);
                writer.Write((ushort)channels);
                writer.Write(frequency);
                writer.Write(frequency * channels * 2);
                writer.Write((ushort)(channels * 2));
                writer.Write((ushort)16);
                writer.Write("data".ToCharArray());
                writer.Write(samples.Length * 2);

                foreach (var sample in samples)
                {
                    writer.Write((short)(sample * short.MaxValue));
                }
            }
            return memoryStream.ToArray();
        }
    }

    private void SendRecording()
    {
        Debug.Log("sending recording");
        text.color = Color.yellow;
        text.text = "Sending...";

        HuggingFaceAPI.AutomaticSpeechRecognition(bytes, response =>
        {
            text.color = Color.white;
            text.text = response;
            startButton.interactable = true;
        }, error =>
        {
            text.color = Color.red;
            text.text = error;
            startButton.interactable = true;
        });

    }

}
