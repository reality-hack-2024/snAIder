using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.Networking;

public class Push : MonoBehaviour
{


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            //Add Push code here
            PushWaypoints();

            //DestroyObjectsWithTag("spawn");
            
            //ClearFileContentFromBallMovers();
        }
    }
    void DestroyObjectsWithTag(string tag)
    {
        GameObject[] objectsToDestroy = GameObject.FindGameObjectsWithTag(tag);

        foreach (var obj in objectsToDestroy)
        {
            Destroy(obj);
        }
    }

    // Example method to clear file content from all game objects with ballMover script
    void ClearFileContentFromBallMovers()
    {
        // Find all game objects with the ballMover script
        ballMover[] ballMovers = FindObjectsOfType<ballMover>();

        // Call the ClearFileContent method for each ballMover
        foreach (var ballMover in ballMovers)
        {
            ballMover.ClearFileContent();
        }
    }

    public void PushWaypoints()
    {

        // Find all game objects with the ballMover script
        ballMover[] ballMovers = FindObjectsOfType<ballMover>();

        // Call the ClearFileContent method for each ballMover
        foreach (var ballMover in ballMovers)
        {
            ballMover.PushWaypoints();
        }
    }
}
