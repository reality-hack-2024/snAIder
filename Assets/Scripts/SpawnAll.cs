using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAll : MonoBehaviour
{
    public GameObject objectPrefab; // The prefab of the object you want to spawn
    public float spawnDistance = 5f; // Distance from the VR camera to spawn objects
    public float delayBeforeSpawn = 2f; // Delay before spawning objects

    private Vector3 spawnPosition;

    void Start()
    {
        spawnPosition = Camera.main.transform.position + Camera.main.transform.forward * spawnDistance;
        Invoke("SpawnObjects", delayBeforeSpawn);
    }

    void SpawnObjects()
    {
            // Instantiate the object at the calculated position
            GameObject instantiatedObject = Instantiate(objectPrefab, spawnPosition, Quaternion.identity);

    }
}
