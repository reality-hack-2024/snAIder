using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
//using HuggingFace.API;

public class WaypointReader : MonoBehaviour
{
    public string fileName = "wayPointData.txt";
    public GameObject ball;
    List<Vector3> waypointsList;
    int wayPointIdx = 0;
    void Start()
    {
        Debug.Log("starting waypointReader");
        waypointsList = new List<Vector3>();
        ReadWaypointsFromFile();
        
    }
    void Update()
    {
        MoveBallToWaypoint(wayPointIdx);
    }
    void ReadWaypointsFromFile()
    {
        Debug.Log("starting ReadWaypointsFromFile");
        string filePath = Path.Combine(Application.dataPath, fileName);

        try
        {
            // Read the file and parse waypoints
            string[] lines = File.ReadAllLines(filePath);

            foreach (string line in lines)
            {
                string[] coordinates = line.Split(',');
                float x = float.Parse(coordinates[0]);
                float y = float.Parse(coordinates[1]);
                float z = float.Parse(coordinates[2]);

                Debug.Log("waypoint x: " +x+"y: "+y+"z: "+z);
                Vector3 waypointVector = new Vector3(x, y, z);
                waypointsList.Add(waypointVector);
            }
        }
        catch (IOException e)
        {
            Debug.LogError("Error reading the file: " + e.Message);
        }
    }

    void MoveBallToWaypoint(int index)
    {
        Vector3 waypoint = waypointsList[index];
        Debug.Log("reaching " + waypoint.x + "" + waypoint.y + " " + waypoint.z);
        float speed = 1.0f;
        // Calculate the distance between the current position and the target waypoint
        float distance = Vector3.Distance(ball.transform.position, waypoint);

        // Move the ball towards the waypoint using Vector3.MoveTowards
        ball.transform.position = Vector3.MoveTowards(ball.transform.position, waypoint, speed * Time.deltaTime);

        // Check if the ball has reached the waypoint
        if (distance < 0.1f)
        {
            wayPointIdx++;
            // Optionally, perform any actions when the ball reaches the waypoint
            Debug.Log("Reached waypoint: " + waypoint);
        }
    }
}