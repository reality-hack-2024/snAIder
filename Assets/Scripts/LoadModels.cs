using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadModels : MonoBehaviour
{
    public GameObject[] objectPrefab; // The prefab of the object you want to instantiate

    public Transform[] spawnPoint;

    public GameObject x;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        if(other.gameObject.tag == "Player")
        {
            Debug.Log("Player " + other.gameObject.name);
            //spawnPoint[0] = gameObject.transform;

            LoadObject(0);
           
            gameObject.SetActive(false);
        }
      
    }
    public void LoadObject(int index)
    {
        if (objectPrefab[index] != null && spawnPoint[index] != null)
        {
            // Instantiate the object at the specified spawn point
            GameObject instantiatedObject = Instantiate(objectPrefab[index], spawnPoint[index].position, Quaternion.identity);
            instantiatedObject.transform.Rotate(-90f, 0f, 0f);
            //x.SetActive(false);
        }
    }
}
