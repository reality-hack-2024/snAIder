
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using Oculus.Interaction.PoseDetection;
//using static UnityEditor.ShaderGraph.Internal.KeywordDependentCollection;
using UnityEngine.UIElements;
using Newtonsoft.Json;
using static Push;
using UnityEngine.Networking;
using OVRSimpleJSON;
using System.Linq;
using TMPro;

public class ballMover : MonoBehaviour
{
    public float circleRadius = 5.0f;
    public string fileName = "waypoint"+sequence+".txt";
    private Vector3 startingPoint;
    public GameObject spawnline;

    private static int sequence = 0;
    private List<Vector3> vectorSequence = new List<Vector3>();

    [System.Serializable]
    public class WaypointsData
    {
        public string Collection;
        public string Object;
        public List<string> Waypoints;
    }

    [System.Serializable]
    public class ApiResponse
    {
        public string Description;
        public string Document_Id;
    }


    void Start()
    {
        sequence++;
        fileName = "waypoint_"+ gameObject.name + sequence + ".txt";
        startingPoint = transform.position;
        //ClearFileContent();
        Debug.Log("Plog filename: " + fileName);
    }
    void Update()
    {
        float distance = CalculateDistance(startingPoint, gameObject.transform.position);
        Debug.Log("sidlog distance: "+ distance);
        if (distance > circleRadius)
        {
            // Spawn a cube at the current position of the ball
            SpawnCube(transform.position);

            // Update the starting point to the current position
            startingPoint = transform.position;

            vectorSequence.Add(startingPoint);
            Debug.Log("plog added vector: " + startingPoint);

            WritePositionToFile(startingPoint);
        }
    }


    float CalculateDistance(Vector3 point1, Vector3 point2)
    {
        // Use Vector3.Distance to calculate the distance between two points
        return Vector3.Distance(point1, point2);
    }

    void SpawnCube(Vector3 position)
    {

        //GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        GameObject instantiatedObject = Instantiate(spawnline, position, Quaternion.identity);
    }
    void WritePositionToFile(Vector3 position)
    {
        // Get the full file path
        string filePath = Path.Combine(Application.dataPath, fileName);


        try
        {
            // Write the position to the file in the format "x, y, z"
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine($"{position.x}, {position.y}, {position.z}");
            }
        }
        catch (IOException e)
        {
            Debug.LogError("Error writing to file: " + e.Message);
        }
    }

    public void ClearFileContent()
    {
        // Get the full file path
        string filePath = Path.Combine(Application.dataPath, fileName);

        Debug.Log("plog before deleting vector: " +fileName + "__"+vectorSequence.Count);
        vectorSequence = new List<Vector3>();
        Debug.Log("plog deleted vector: " + fileName + "__" + vectorSequence.Count);
        try
        {
            // Create or overwrite the file to clear its content
            File.WriteAllText(filePath, string.Empty);
        }
        catch (IOException e)
        {
            Debug.LogError("Error clearing file content: " + e.Message);
        }
    }



    private string apiUrl = "https://snaider-api-ed5f314b723f.herokuapp.com/save_waypoints"; // Replace with your API URL
    private string jsonData;


    public void PushWaypoints()
    {
        // Convert the list of Vector3 to a list of formatted strings
        List<string> formattedWaypoints = ConvertVector3ListToStringList(vectorSequence);
        Debug.Log("klog: formatted Waypoints" + formattedWaypoints.Count);
        // Create an instance of the WaypointsData class
        WaypointsData waypointsData = new WaypointsData
        {
            Collection = "scripts",
            Object = gameObject.name, // Replace this with the actual object name
            Waypoints = formattedWaypoints
        };

        // Convert the waypointsData to JSON format
        string json = JsonConvert.SerializeObject(waypointsData);

        Debug.Log("klog: "+json);

        if(formattedWaypoints.Count == 0)
        {
            Debug.Log("klog: json empty");
        }
        else
        {
            //code to push to api url
            jsonData = json;
            StartCoroutine(PostDataToAPI());
        }


    }

    List<string> ConvertVector3ListToStringList(List<Vector3> vectorList)
    {
        List<string> stringList = new List<string>();

        foreach (Vector3 vector in vectorList)
        {
            Debug.Log("klog: for each v3" + vector);
            string formattedVector = $"{vector.x},{vector.y},{vector.z}";
            stringList.Add(formattedVector);
        }

        return stringList;
    }

    IEnumerator PostDataToAPI()
    {
        // Create a UnityWebRequest
        using (UnityWebRequest request = UnityWebRequest.PostWwwForm(apiUrl, "POST"))
        {
            // Set the request headers
            request.SetRequestHeader("Content-Type", "application/json");

            // Convert the JSON string to bytes
            byte[] jsonBytes = System.Text.Encoding.UTF8.GetBytes(jsonData);
            request.uploadHandler = new UploadHandlerRaw(jsonBytes);

            // Set up the download handler to receive the response
            request.downloadHandler = new DownloadHandlerBuffer();


            // Send the request
            yield return request.SendWebRequest();

            // Check for errors
            if (request.result == UnityWebRequest.Result.Success)
            {
                Debug.Log("klog JSON data sent successfully");

                // Get the response text
                string responseText = request.downloadHandler.text;
                Debug.Log($"klog API Response: {responseText}");

                // Deserialize the JSON response into the ApiResponse class
                ApiResponse apiResponse = JsonUtility.FromJson<ApiResponse>(responseText);
                // Find the UI TextMeshPro element with the specified tag
                TextMeshProUGUI textMeshPro = FindUITextWithTag("text");
                // Check if the TextMeshPro element is found
                if (textMeshPro != null)
                {
                    // Replace the text with your desired content
                    textMeshPro.text += apiResponse.Description;
                    Debug.LogWarning("kkk "+ apiResponse.Description);
                }
                else
                {
                    Debug.LogWarning($"kkk No UI TextMeshPro element found with tag: {"text"}");
                }

                // Access the properties of the deserialized object
                Debug.Log($"kk Description: {apiResponse.Description}");

            }
            else
            {
                Debug.LogError($"klog Error sending JSON data: {request.error}");
            }
        }
    }

    TextMeshProUGUI FindUITextWithTag(string tag)
    {
        // FindObjectsOfType<TextMeshProUGUI> returns an array of all TextMeshProUGUI elements in the scene
        // Here, we filter the elements based on the specified tag
        TextMeshProUGUI[] textMeshPros = FindObjectsOfType<TextMeshProUGUI>();

        foreach (TextMeshProUGUI textMeshPro in textMeshPros)
        {
            if (textMeshPro.CompareTag(tag))
            {
                return textMeshPro;
            }
        }

        return null; // Return null if no matching element is found
    }

}