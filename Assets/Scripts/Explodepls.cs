using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explodepls : MonoBehaviour
{

    public ParticleSystem particleEffect;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Finish")
        {
            if (particleEffect != null)
            {
                Vector3 collisionPoint = collision.contacts[0].point;
                Quaternion collisionRotation = Quaternion.LookRotation(collision.contacts[0].normal);

                ParticleSystem instantiatedEffect = Instantiate(particleEffect, collisionPoint, collisionRotation);
                instantiatedEffect.Play();

                // Destroy the collided object
                Destroy(gameObject);
            }
        }
    }
}
