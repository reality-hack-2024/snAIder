# SnAIder Cut

![alt text](https://codeberg.org/reality-hack-2024/TABLE_62/media/branch/main/media/title.png)

We are using Mixed Reality and Generative AI to make coordinating movie scenes easier than ever by allowing creatives to collaborate on pre-production planning and reduce delays in filming

Demo video YT: https://www.youtube.com/watch?v=BFUc7yhYLQQ&t=3s
Demo video GitHub: https://codeberg.org/reality-hack-2024/TABLE_62/media/branch/main/media/snaider_demo3.mp4


## UPDATE: 
This project won **Best Location AR with LightShip ARDK** project at MIT XR Reality Hackathon! See the list of winners here: https://mit-reality-hack-2024.devpost.com/project-gallery 


## Architecture
![alt text](https://codeberg.org/reality-hack-2024/TABLE_62/media/branch/main/media/arch_snAIder.png)


### **Inspiration**
We wanted to create a collaborative tool that let multiple people be involved in the creative process of design. We wanted to make communication a visual experience and not just written

### **What it does**
Our platform allows screen-writers to generate scenes in mixed-reality and convert them into a script. Changes to the script are reflected back in a replayable MR experience

### **How we built it**
We used Meshy.ai to generate custom images using voice commands and used LLMs to convert scenes into scripts and vice-versa. Our platform runs on Unity and Quest

### **Challenges we ran into**
Describing a scene as a set of animations or datapoints involved imagination

### **Accomplishments that we're proud of**
We were able to get the end-2-end process of converting movement into scripts, modifying those scripts and loading those changes into the headset in real time

### **What we learned**
Using Rest APIs in Unity for multiple services allowed us to create experiences outside the Unity editor

### **What's next for snAIder**
Multiplayer for collaboration
Implementing more complicated scenes involving many more objects

Find the pitch deck here: https://lnkd.in/gF9cAkf7

## Description
### Play anywhere:
Block out a scene in your living room or a public park. Then using Niantic Lightship, play back the scene in popular places that Niantic has already mapped out. This way you see what your scene will look like on-location before filming.

### Speech to Model:
No need to load or create place-holder models ahead of time. Using Hugging Face's Speech-to-text APIs and Meshy's prompt to 3D model API, we were able to generate custom 3D models on the fly. Just ask for a motorcycle, a tree, or a dinosaur, and it's there!

### Action to Text:
Record the movement of your 3D models and replay them so that you can share your vision of a scene with your production crew. How does your car chase scene play out? Do you zig zag around trees, go around them, or crash? We converted the movements made by the user into textual descriptions using LLMs. "The motorcycle gracefully jumps off a ramp and soars through the air before landing safely." - a real generated response from our live demo. Now you have a script!

### Text to Action
This is the one I spent the most time working on. Using the original script generated from the Action to Text model, we can change the movement of the 3D objects by editing the text itself. Instructing the LLM to modify the movement of our motorcycle to "Circle the ramp" Instead of. "jumping off a ramp" results in that exact action in the augmented reality space. This means that one can edit a scene directly in 3D or through changes to a text document! There is a 1-1 relationship between the two modes of communication.

SnAIder (Cut Pro) was certainly a hodgepodge of different systems, but connecting them together (at the last minute) created such a unique experience that showcases what the future of XR might hold.
## Setup

Transfer the apk file called 2.zip to your Quest 3 and install the app.

LightshipVPS_DATA.zip contains the code files we used for the niantic lightship VPS integration part.

snAIder_flask.zip contains the code files used to make a custom ChatGPT API for script generation.

## Run

1. Unzip 2.zip
   - Transfer the 2.apk to your Quest device
     - Install the apk
2. Play with a controller


## Shout-Outs

Meshy AI - https://www.meshy.ai/blog/meshy-1-generate-3d-models-with-ai-in-just-a-minute

Meta Presence Platform - https://developers.facebook.com/blog/post/2023/04/25/presence-platform-overview/

Niantic Lightship VPS - https://lightship.dev/products/vps 

Shout out to Barnabas Lee. He was an amazing mentor. https://www.linkedin.com/in/barnabaslee/


